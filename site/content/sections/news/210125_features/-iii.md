title:      No export
date:       2021.03.25 15:00
published:  true
comments:   false
image:      
~~~
A dash (-) in front of a file name prevents the file from processing.
~~~
If you don't want to publish content, simply add a dash (-) in front of the filename, or folder name.

And if a content folder starts with a dash, like `.exclude/`, all its content gets excluded.
