title:      Overview of features
date:       2021.01.25 14:52
published:  true
comments:   false
image:      cover_.png
~~~
Here is an overview of many of Jongleur's feature.
~~~


## JTAGs

Jongleur TAGs can be used for different features.

For a full description of the JTAGs, see [e:config.json](https://gitlab.com/pqq/jongleur/-/blob/master/example_site/config.cson).


#### LEC - Load External Content

Code:  
&#123;&#123;`LEC=https://gitlab.com/pqq/jongleur/-/raw/master/example_site/content/sections/-noexport/theDead.md`&#125;&#125;

Output:  
```
{{LEC=https://gitlab.com/pqq/jongleur-example/-/raw/main/site/content/sections/-noexport/thisWillNotBeExported.md}}
```


#### LIP - Load Internal Post

Code:  
&#123;&#123;`LIP=/content/sections/news/210125_features/.ii.md`&#125;&#125;

Output:  
```
{{LIP=/content/sections/news/210125_features/.ii.md}}
```


#### LSP - Load Section Posts

Note: `LSp` is identical to `LSP` except for the fact that hidden posts are excluded.

Code:  
&#123;&#123;`LSP-2=books`&#125;&#125;

Output:  
{{LSP-2=books}}

<span style="padding-top: 200px;">&nbsp;</span>


#### LSG - Load Section as Grid

Note: `LSg` is identical to `LSG` except for the fact that hidden posts are excluded.

Code:  
&#123;&#123;`LSG=books`&#125;&#125;

Output:  
{{LSG=books}}

<span style="padding-top: 200px;">&nbsp;</span>


## Images

Photos are Automatically rescaled, and watermarks are added, with the built in "photoPrepper" tool.

Code: `![](image.jpg)`
Output:  
![](image.jpg)

Avoid "photo prepping" by adding a trailing underscore to the name of the file.

Code: `![](image2_.jpg)`
Output:  
![](image2_.jpg)


## Links

#### External links

Code: `[e:External link to mpath.ink](https://mpath.ink)`
Output: [e:External link to mpath.ink](https://mpath.ink)

#### Internal links

Internal link to hidden post.

Code: `[i:Link to hidden post](.hidden/hardtofind/i.md)`
Output: [i:Link to hidden post](.hidden/hardtofind/i.md)

#### CDN links

Open URL using Content Delivery Network

Code: `[c:Load resource via CDN](pqq/jongleur/master/templates/layout/index.html)`
Output: [c:Load resource via CDN](pqq/jongleur/master/templates/layout/index.html)
