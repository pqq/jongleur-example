title:      Children of Time
date:       2021.03.15 15:55
published:  true
comments:   false
image:      ../assets/chair.jpg
~~~
by Adrian Tchaikovsky
~~~

![](cot_.jpg)

*A race for survival among the stars... Humanity's last survivors escaped earth's ruins to find a new home. But when they find it, can their desperation overcome its dangers?*

Read more at [e:goodreads.com](https://www.goodreads.com/book/show/25499718-children-of-time)
