title:      Nineteen Eighty-Four
date:       2021.03.15 16:05
published:  true
comments:   false
image:      coverImage_.jpg
~~~
by George Orwell
~~~

![1984 cover image](coverImage_.jpg)

*Among the seminal texts of the 20th century, Nineteen Eighty-Four is a rare work that grows more haunting as its futuristic purgatory becomes more real. Published in 1949, the book offers political satirist George Orwell's nightmarish vision of a totalitarian, bureaucratic world and one poor stiff's attempt to find individuality. The brilliance of the novel is Orwell's prescience of modern life—the ubiquity of television, the distortion of the language—and his ability to construct such a thorough version of hell. Required reading for students since it was published, it ranks among the most terrifying novels ever written.*

Read more at [e:goodreads.com](https://www.goodreads.com/book/show/40961427-1984)
