title:      I am hard to find
date:       2021.03.15 15:47
published:  true
comments:   false
image:      
~~~
This content will not be listed, so it is a bit harder to find
~~~
The reason for this is that the parent's parent folder starts with a dot (.), as the name is `.hidden`. You will need the direct link to view me.

[e:mzfo]([assets]/mzfo.html)
